﻿using System.IO;
using System.Reflection;

namespace Lima.ML.Regression {

    public class DotNetExtensions {

        public static string GetAbsolutePath(string relativePath) {

            FileInfo _dataRoot = new FileInfo(Assembly.GetExecutingAssembly().Location);

            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.GetFullPath(Path.Combine(assemblyFolderPath, relativePath));

            return fullPath;
        }
    }
}
