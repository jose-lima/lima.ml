﻿using Lima.ML.Regression.DataStructures;
using Microsoft.ML;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Lima.ML.Regression {

    public static class ModelScoringTester {

        public static void VisualizeSomePredictions(MLContext mlContext,
                                                    string modelName,
                                                    string testDataLocation,
                                                    PredictionEngine<InputDto, OutputDto> predEngine,
                                                    int numberOfPredictions) {
            //Make a few prediction tests 
            // Make the provided number of predictions and compare with observed data from the test dataset
            var testData = ReadSampleDataFromCsvFile(testDataLocation, numberOfPredictions);

            for (int i = 0; i < numberOfPredictions; i++) {
                //Score
                var resultprediction = predEngine.Predict(testData[i]);


                ConsoleHelper.PrintRegressionPredictionVersusObserved(resultprediction.Prediction.ToString(),
                                                            testData[i].Label.ToString());
            }

        }

        //This method is using regular .NET System.IO.File and LinQ to read just some sample data to test/predict with 
        public static List<InputDto> ReadSampleDataFromCsvFile(string dataLocation, int numberOfRecordsToRead) {
            return File.ReadLines(dataLocation)
                .Skip(1)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Split(','))
                .Select(x => new InputDto() {
                    Label = float.Parse(x[0], CultureInfo.InvariantCulture),
                    Feat1 = float.Parse(x[1], CultureInfo.InvariantCulture),
                    Feat2 = float.Parse(x[2], CultureInfo.InvariantCulture),
                })
                .Take(numberOfRecordsToRead)
                .ToList();
        }
    }
}
