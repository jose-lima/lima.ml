﻿namespace Lima.ML.Regression {

    internal static class Program {

        
        private static string DatasetsLocation = @"../../Data";
        private static string TrainingDataRelativePath = $"{DatasetsLocation}/hour_train.csv";
        private static string TestDataRelativePath = $"{DatasetsLocation}/hour_test.csv";
        private static string TrainingDataFilePath = DotNetExtensions.GetAbsolutePath(TrainingDataRelativePath);
        private static string TestDataFilePath = DotNetExtensions.GetAbsolutePath(TestDataRelativePath);


        static void Main(string[] args) {

            var regressor = new MultipleRegressor(TrainingDataFilePath, TestDataFilePath);
            regressor.TrainAllModels();
            regressor.Predict();

            ConsoleHelper.ConsolePressAnyKey();
        }


    }
}
