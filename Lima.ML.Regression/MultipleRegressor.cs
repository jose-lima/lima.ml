﻿using Lima.ML.Regression.DataStructures;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.IO;

namespace Lima.ML.Regression {

    public class MultipleRegressor {

        private static string ModelsLocation = @"../../MLModels";

        private MLContext _mlContext;

        public string TrainingDataFilePath { get; }
        public string TestDataFilePath { get; }

        private (string name, IEstimator<ITransformer> value)[] EstimationModels;

        private EstimatorChain<ColumnConcatenatingTransformer> _concatFeatures;
        private IDataView TrainingDataView;
        private IDataView TestDataView;

        public MultipleRegressor(string trainingDataFilePath, string testDataFilePath) {

            TrainingDataFilePath = trainingDataFilePath;
            TestDataFilePath = testDataFilePath;

            // Create MLContext to be shared across the model creation workflow objects 
            // Set a random seed for repeatable/deterministic results across multiple trainings.
            _mlContext = new MLContext(seed: 0);
            RegisterRegressionModels(_mlContext);


            // 1. Common data loading configuration
            TrainingDataView = LoadDataFromCsv(_mlContext, trainingDataFilePath);
            TestDataView = LoadDataFromCsv(_mlContext, testDataFilePath);


            // 2. Common data pre-process with pipeline data transformations
            // Concatenate all the numeric columns into a single features column
            _concatFeatures = _mlContext.Transforms.Concatenate("Features",
                nameof(InputDto.Feat1),
                nameof(InputDto.Feat2))
                    .AppendCacheCheckpoint(_mlContext);

            // Use in-memory cache for small/medium datasets to lower training time. 
            // Do NOT use it (remove .AppendCacheCheckpoint()) when handling very large datasets.

            // (Optional) Peek data in training DataView after applying the ProcessPipeline's transformations  
            ConsoleHelper.PeekDataViewInConsole(_mlContext, TrainingDataView, _concatFeatures, 10);
            ConsoleHelper.PeekVectorColumnDataInConsole(_mlContext, "Features", TrainingDataView, _concatFeatures, 10);

        }

        private void RegisterRegressionModels(MLContext mlContext) {

            // Definition of regression trainers/algorithms to use
            //var regressionLearners = new (string name, IEstimator<ITransformer> value)[]
            EstimationModels = new (string name, IEstimator<ITransformer> value)[]
            {
                ("FastTree", mlContext.Regression.Trainers.FastTree()),
                ("Poisson", mlContext.Regression.Trainers.LbfgsPoissonRegression()),
                //("GAM", mlContext.Regression.Trainers.Gam()),
                //("SDCA", mlContext.Regression.Trainers.Sdca()),
                //("FastTreeTweedie", mlContext.Regression.Trainers.FastTreeTweedie()),
                //Other possible learners that could be included
                //...FastForestRegressor...
                //...GeneralizedAdditiveModelRegressor...
                //...OnlineGradientDescent... (Might need to normalize the features first)
            };
        }

        // 4. Try/test Predictions with the created models
        // The following test predictions could be implemented/deployed in a different application (production apps)
        // that's why it is seggregated from the previous loop
        // For each trained model, test 10 predictions     
        public void Predict() {

            foreach (var estimationModel in EstimationModels) {

                Predict(estimationModel);
            }
        }

        private void Predict((string name, IEstimator<ITransformer> value) estimationModel) {

            //Load current model from .ZIP file 
            string modelRelativeLocation = $"{ModelsLocation}/{estimationModel.name}Model.zip";
            string modelPath = DotNetExtensions.GetAbsolutePath(modelRelativeLocation);
            ITransformer trainedModel = _mlContext.Model.Load(modelPath, out var modelInputSchema);

            // Create prediction engine related to the loaded trained model
            var predEngine = _mlContext.Model.CreatePredictionEngine<InputDto, OutputDto>(trainedModel);

            Console.WriteLine($"================== Visualize/test 10 predictions for model {estimationModel.name}Model.zip ==================");
            //Visualize 10 tests comparing prediction with actual/observed values from the test dataset
            ModelScoringTester.VisualizeSomePredictions(_mlContext, estimationModel.name, TestDataFilePath, predEngine, 6);
        }

        // 3. Phase for Training, Evaluation and model file persistence
        // Per each regression trainer: Train, Evaluate, and Save a different model
        public void TrainAllModels() {

            foreach (var estimationModel in EstimationModels) {

                TrainModel(estimationModel);
            }
        }

        private void TrainModel((string name, IEstimator<ITransformer> value) estimationModel) {

            Console.WriteLine("=============== Training the current model ===============");
            var trainingPipeline = _concatFeatures.Append(estimationModel.value);
            var trainedModel = trainingPipeline.Fit(TrainingDataView);

            Console.WriteLine("===== Evaluating Model's accuracy with Test data =====");
            IDataView predictions = trainedModel.Transform(TestDataView);

            var metrics = _mlContext.Regression.Evaluate(
                data: predictions, 
                labelColumnName: nameof(InputDto.Label), 
                scoreColumnName: "Score");

            ConsoleHelper.PrintRegressionMetrics(estimationModel.value.ToString(), metrics);

            //Save the model file that can be used by any application
            string modelRelativeLocation = $"{ModelsLocation}/{estimationModel.name}Model.zip";
            string modelPath = DotNetExtensions.GetAbsolutePath(modelRelativeLocation);
            if (!Directory.Exists(Path.GetDirectoryName(modelPath))) {
                Directory.CreateDirectory(modelPath);
            }
            _mlContext.Model.Save(trainedModel, TrainingDataView.Schema, modelPath);
            Console.WriteLine("The model is saved to {0}", modelPath);
        }

        private IDataView LoadDataFromCsv(MLContext mlContext, string dataFilePath) {

            return mlContext.Data.LoadFromTextFile<InputDto>(
                path: dataFilePath,
                hasHeader: true,
                separatorChar: ',');
        }

    }
}
