﻿
using Microsoft.ML.Data;

namespace Lima.ML.Regression.DataStructures {

    public class OutputDto {

        [ColumnName("Score")]
        public float Prediction;

    }
}
