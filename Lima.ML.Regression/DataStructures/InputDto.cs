﻿
using Microsoft.ML.Data;

namespace Lima.ML.Regression.DataStructures {

    public class InputDto {

        [LoadColumn(0)]
        public float Label { get; set; }

        [LoadColumn(1)]
        public float Feat1 { get; set; }

        [LoadColumn(2)]
        public float Feat2 { get; set; }
    }

    public static class DemandObservationSample {

        public static InputDto SingleInputSample =>

            new InputDto() {
                Label = 100,
                Feat1 = 0,
                Feat2 = 1,
            };
    }
}
