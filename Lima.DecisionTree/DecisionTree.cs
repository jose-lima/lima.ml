﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Lima.DecisionTree.Stat;

namespace Lima.DecisionTree {

    public class DecisionTree {

        private int _featColumn;
        private int _labelColumn;
        private double _maxAvgAbsoluteLoss;
        private static readonly ILossFunction LossFunction = new MeanSquareRoot();

        public DecisionTree(IEnumerable<double[]> dataSet, int featColumn, int labelColumn, double maxAvgAbsoluteLoss) {
            _featColumn = featColumn;
            _labelColumn = labelColumn;
            _maxAvgAbsoluteLoss = maxAvgAbsoluteLoss;
            Data = dataSet.ToArray();
        }

        public void Train() {
            Data = Data.OrderBy(x => x[_featColumn]).ToArray();
            TrainRecur();
        }

        private void TrainRecur() {

            var splitResults =
                new Dictionary<double, (double splitAvgLoss, DecisionTree leftNode, DecisionTree rightNode)>(Data.Count());

            for (var i = 0; i < Data.Count() - 1; i++) {
                var splitPosition = (Data[i + 1][_featColumn] + Data[i][_featColumn]) / 2;
                splitResults[splitPosition] = GetSplitLoss(Data, splitPosition);
            }

            var min = splitResults.OrderBy(x => x.Value.splitAvgLoss).First();

            Split = min.Key;
            TotAbsoluteLoss = Math.Round(min.Value.splitAvgLoss, 3);

            //Recurr left
            LeftNode = min.Value.leftNode;
            if (LeftNode.AvgAbsoluteLoss > _maxAvgAbsoluteLoss) {
                LeftNode.TrainRecur();
            }

            //Recurr right
            RightNode = min.Value.rightNode;
            if (RightNode.AvgAbsoluteLoss > _maxAvgAbsoluteLoss) {
                RightNode.TrainRecur();
            }
        }

        public double[][] Data { get; private set; }

        public double Avg { get; private set; }

        public double TotAbsoluteLoss { get; private set; }

        public double? Split { get; private set; }

        public DecisionTree LeftNode { get; private set; }

        public DecisionTree RightNode { get; private set; }

        public double AvgAbsoluteLoss {

            get {

                if (Split == null) {
                    return TotAbsoluteLoss / Count;
                }

                var leftLossPart = LeftNode.AvgAbsoluteLoss * ((double)LeftNode.Count / Count);
                var rightLossPart = RightNode.AvgAbsoluteLoss * ((double)RightNode.Count / Count);

                return leftLossPart + rightLossPart;
            }
        }

        public int Count => Data.Count();

        private (double splitAvgLoss, DecisionTree leftNode, DecisionTree rightNode) GetSplitLoss(
            double[][] dataSet,
            double split) {

            var (leftData, rightData) = SplitDataSet(dataSet, split);

            //Left
            var (leftTotLoss, leftAvg) = GetTotLossToAvg(leftData, _labelColumn, LossFunction);
            var leftNode = new DecisionTree(leftData, _featColumn, _labelColumn, _maxAvgAbsoluteLoss) {
                Avg = leftAvg,
                TotAbsoluteLoss = leftTotLoss
            };

            //Right
            var (rightTotLoss, rightAvg) = GetTotLossToAvg(rightData, _labelColumn, LossFunction);
            var rightNode = new DecisionTree(rightData, _featColumn, _labelColumn, _maxAvgAbsoluteLoss) {
                Avg = rightAvg,
                TotAbsoluteLoss = rightTotLoss
            };

            var splitAvgLoss = (leftTotLoss + rightTotLoss) / dataSet.Count();

            return (splitAvgLoss, leftNode, rightNode);
        }


        private (IEnumerable<double[]>, IEnumerable<double[]>) SplitDataSet(double[][] dataSet, double splitPosition) {

            var left = dataSet.Where(x => x[_featColumn] <= splitPosition);
            var right = dataSet.Where(x => x[_featColumn] > splitPosition);
            return (left, right);
        }

        public double[] Predict(double[] input) {

            return input.Select(Predict).ToArray();
        }

        public double Predict(double input) {

            if (Split == null) {
                return Avg;
            }

            if (input < Split) {
                return LeftNode.Predict(input);
            }

            return RightNode.Predict(input);
        }

        public override string ToString() {

            if (Split == null) {
                return $"{GetType().Name}, {nameof(Avg)}: {Avg}, {nameof(AvgAbsoluteLoss)}: {AvgAbsoluteLoss}";
            }

            return $"{GetType().Name}, {nameof(Split)}: {Split}, {nameof(AvgAbsoluteLoss)}: {AvgAbsoluteLoss}, {nameof(LeftNode)}: {LeftNode}, {nameof(RightNode)}: {RightNode}";
        }
    }
}
