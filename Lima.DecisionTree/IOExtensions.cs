﻿using System.IO;
using System.Linq;

namespace Lima.DecisionTree {

    public static class IOExtensions {

        public static double[][] ReadCsv(string filePath) {

            var textLines = File.ReadAllLines(filePath);
            var dataRows = textLines
                .Skip(1) //header
                .Select(x => ParseTextLine(x))
                .ToArray();
            return dataRows;
        }

        private static double[] ParseTextLine(string delimitedString) {

            var separator = ',';
            var dataRow = delimitedString.Split(separator).Select(x => double.Parse(x)).ToArray();
            return dataRow;
        }
    }
}
