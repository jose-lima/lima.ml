﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lima.DecisionTree {

    public static class Stat {

        public interface ILossFunction {

            double CalTotLoss(double[] y, double[] yHat);
        }

        public class MeanSquareRoot : ILossFunction {

            public double CalTotLoss(double[] y, double[] yHat) {

                if (y.Count() != yHat.Count()) {
                    throw new ArgumentException("y and yHat must have same count.");
                }

                var loss = 0.0;
                for (var i = 0; i < y.Count(); i++) {
                    var absError = Math.Sqrt(Math.Pow(y[i] - yHat[i], 2));
                    loss += absError;
                }

                return loss;
            }
        }

        public static (double totLoss, double avg) GetTotLossToAvg(
            IEnumerable<double[]> dataSet,
            int labelColumn,
            ILossFunction lossFunction) {

            var y = dataSet.Select(x => x[labelColumn]).ToArray();

            var avg = dataSet.Select(x => x[labelColumn]).Average();
            var yHat = Enumerable.Range(0, dataSet.Count()).Select(x => avg).ToArray();

            var totLoss = lossFunction.CalTotLoss(y, yHat);

            return (totLoss, avg);
        }

        public static (double[][], double[][]) SplitTrainTest(
            double[][] inputData,
            double percentageTrain,
            int randSeed) {

            var totSamples = inputData.Count();
            var rand = new Stat.NonRepeatingRand(randSeed, 0, totSamples);

            //train
            var trainSampCount = (int)(totSamples * percentageTrain);
            var trainSet = Enumerable.Range(0, trainSampCount).ToList()
                .Select(_ => inputData[rand.Next()])
                .ToArray();

            //test
            var testSampCount = totSamples - trainSampCount;
            var testSet = Enumerable.Range(0, testSampCount).ToList()
                .Select(_ => inputData[rand.Next()])
                .ToArray();


            return (trainSet, testSet);
        }

        public static (double[][] data, double min, double span) StandardScore(double[][] input, int column) {

            var (data, min, span) = Normalize(input.Select(x => x[column]));
            var normData = data.ToArray();
            var i = 0;
            var normalizedDataSet = input.Select(x => {

                var newRow = new double[x.Length];
                x.CopyTo(newRow, 0);
                newRow[column] = normData[i];
                i++;
                return newRow;

            }).ToArray();

            return (normalizedDataSet, min, span);
        }

        public static (IEnumerable<double> data, double min, double span) Normalize(IEnumerable<double> input) {

            var min = input.Min();
            var max = input.Max();
            var span = max - min;

            var normalizedData = input.Select(x => Normalize(x, min, span));

            return (normalizedData, min, span);
        }

        public static double Normalize(double input, double min, double span) {
            return (input - min) / span;
        }

        public static double DeNorm(double normalized, double min, double span) {

            return (normalized * span) - min;
        }

        public class NonRepeatingRand {

            private Random _rand;
            private int _inclusiveMin;
            private int _exclusiveMax;
            private HashSet<int> _given;
            private int _count;

            public NonRepeatingRand(int seed, int inclusiveMin, int exclusiveMax) {
                _rand = new Random(seed);
                _inclusiveMin = inclusiveMin;
                _exclusiveMax = exclusiveMax;
                _count = exclusiveMax - inclusiveMin;
                _given = new HashSet<int>(_count);
            }

            public int Next() {

                if (_given.Count == _count) {
                    throw new InvalidOperationException("All random numbers have been retrieved.");
                }

                var rand = _rand.Next(_inclusiveMin, _exclusiveMax);
                while (_given.Contains(rand)) {
                    rand = _rand.Next(_inclusiveMin, _exclusiveMax);
                }
                _given.Add(rand);
                return rand;
            }
        }
    }
}
