﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.DecisionTree {

    public static class ConsoleExtensions {

        public static void Print(double[][] inputData, int max = -1) {

            var counter = 0;

            foreach (var row in inputData) {

                Print(row);

                counter++;

                if (max != -1 && counter >= max) {
                    return;
                }

            }
        }

        public static void Print(double[] inputData) {

            Console.WriteLine(string.Join(", ", inputData));
        }


    }
}
