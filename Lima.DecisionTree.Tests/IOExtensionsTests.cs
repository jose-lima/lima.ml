﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Lima.DecisionTree.Tests {

    [TestClass]
    public class IOExtensionsTests {

        [TestMethod]
        public void ReadCsv() {

            var inputData = IOExtensions.ReadCsv(@"TestData\SingleVarTwoStepWiseConstant.csv");

            //0: -1,0
            //7: 11,3
            Assert.AreEqual(8, inputData.Count());
            Assert.AreEqual(-1, inputData.First()[0]);
            Assert.AreEqual(0, inputData.First()[1]);
            Assert.AreEqual(11, inputData.Last()[0]);
            Assert.AreEqual(3, inputData.Last()[1]);
        }
    }
}
