﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lima.DecisionTree.Tests {

    [TestClass]
    public class LossFunctionsTests {

        [TestMethod]
        public void MeanQuareRoot() {

            var meanQuareRoot = new Stat.MeanSquareRoot();

            var y = new double[] { 1, 2, 3, 4 };

            var yHat = new double[] { 2, 3, 4, 5 };     
            var loss = meanQuareRoot.CalTotLoss(y, yHat);
            Assert.AreEqual(4, loss);

            yHat = new double[] { 3, 4, 5, 6 };
            loss = meanQuareRoot.CalTotLoss(y, yHat);
            Assert.AreEqual(8, loss);

        }
    }
}
