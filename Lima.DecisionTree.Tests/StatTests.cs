﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Lima.DecisionTree.Tests {

    [TestClass]
    public class StatTests {

        [TestMethod]
        public void Normalize_col1_noOffset() {

            var inputData = new double[][] {
                new[]{0.0, 0.0},
                new[]{0.0, 1.0},
                new[]{0.0, 2.0},
                new[]{0.0, 10.0},
            };

            var collumn = 1;
            var (normData, min, span) = Stat.StandardScore(inputData, collumn);

            Assert.AreEqual(4, normData.Count());
            Assert.AreEqual(0, min);
            Assert.AreEqual(10, span);
            Assert.AreEqual(0.0, normData[0][collumn]);
            Assert.AreEqual(0.1, normData[1][collumn]);
            Assert.AreEqual(0.2, normData[2][collumn]);
            Assert.AreEqual(1.0, normData[3][collumn]);           
        }

        [TestMethod]
        public void Normalize_col1_offset() {

            var inputData = new double[][] {
                new[]{0.0, -1.0},
                new[]{0.0, 0.0},
                new[]{0.0, 1.0},
                new[]{0.0, 9.0},
            };

            var collumn = 1;
            var (normData, min, span) = Stat.StandardScore(inputData, collumn);

            Assert.AreEqual(4, normData.Count());
            Assert.AreEqual(-1.0, min);
            Assert.AreEqual(10, span);
            Assert.AreEqual(0.0, normData[0][collumn]);
            Assert.AreEqual(0.1, normData[1][collumn]);
            Assert.AreEqual(0.2, normData[2][collumn]);
            Assert.AreEqual(1.0, normData[3][collumn]);
        }

        [TestMethod]
        public void DeNorm() {

            var inputData = 1;
            var min = 0;
            var span = 10;
            var normData = Stat.Normalize(inputData, min, span);

            Assert.AreEqual(0.1, normData);

            var deNormData = Stat.DeNorm(normData, min, span);

            Assert.AreEqual(inputData, deNormData);
        }
    }
}
