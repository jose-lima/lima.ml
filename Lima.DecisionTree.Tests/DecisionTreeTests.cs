﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Linq;

namespace Lima.DecisionTree.Tests {

    [TestClass]
    public class DecisionTreeTests {

        [TestMethod]
        public void Train_predict_single_var_single_split_tests() {

            //Load data
            var inputData = IOExtensions.ReadCsv(@"TestData\SingleVarTwoStepWiseConstant.csv");

            Console.WriteLine("All data:");
            ConsoleExtensions.Print(inputData);
            Console.WriteLine();

            //Split data sets
            var (trainSet, testSet) = Stat.SplitTrainTest(inputData, 0.7, 1);

            Console.WriteLine("Train data:");
            ConsoleExtensions.Print(trainSet);
            Console.WriteLine();

            Console.WriteLine("Test data:");
            ConsoleExtensions.Print(testSet);
            Console.WriteLine();

            //Decision tree
            var decisionTree = new DecisionTree(trainSet, featColumn: 1, labelColumn: 0, maxAvgAbsoluteLoss: 1);

            var stopWatch = Stopwatch.StartNew();
            decisionTree.Train();
            stopWatch.Stop();
            Console.WriteLine($"Training took {stopWatch.ElapsedMilliseconds}ms");

            Assert.AreEqual(1.5, decisionTree.Split.Value, 0.2);
            Assert.AreEqual(0, decisionTree.LeftNode.Avg, 0.2);
            Assert.AreEqual(10, decisionTree.RightNode.Avg, 0.2);
            Assert.AreEqual(0.7, decisionTree.TotAbsoluteLoss, 0.2);


            var predictions = decisionTree.Predict(testSet.Select(x => x[1]).ToArray());

            Console.WriteLine($"Predictions with model [{decisionTree}]:");
            ConsoleExtensions.Print(predictions);
            Console.WriteLine();

            Assert.AreEqual(0, predictions[0], decisionTree.TotAbsoluteLoss);
            Assert.AreEqual(10, predictions[1], decisionTree.TotAbsoluteLoss);
            Assert.AreEqual(10, predictions[2], decisionTree.TotAbsoluteLoss);
        }

        [TestMethod]
        public void Train_predict_single_var_two_split_tests() {

            //Load data
            var inputData = IOExtensions.ReadCsv(@"TestData\SingleVarThreeStepWiseConstant.csv");

            Console.WriteLine("All data:");
            ConsoleExtensions.Print(inputData);
            Console.WriteLine();

            //Split data sets
            var (trainSet, testSet) = Stat.SplitTrainTest(inputData, 0.7, 1);

            Console.WriteLine("Train data:");
            ConsoleExtensions.Print(trainSet);
            Console.WriteLine();

            Console.WriteLine("Test data:");
            ConsoleExtensions.Print(testSet);
            Console.WriteLine();

            //Decision tree
            var decisionTree = new DecisionTree(trainSet, featColumn: 1, labelColumn: 0, maxAvgAbsoluteLoss: 1);

            var stopWatch = Stopwatch.StartNew();
            decisionTree.Train();
            stopWatch.Stop();
            Console.WriteLine($"Training took {stopWatch.ElapsedMilliseconds}ms");

            Assert.AreEqual(4, decisionTree.Split.Value, 0.2);
            Assert.AreEqual(1.5, decisionTree.LeftNode.Split.Value, 0.2);
            Assert.AreEqual(0, decisionTree.LeftNode.LeftNode.Avg, 0.5);
            Assert.AreEqual(10, decisionTree.LeftNode.RightNode.Avg, 0.2);
            Assert.AreEqual(-50, decisionTree.RightNode.Avg, 0.3);
            Assert.AreEqual(0.7, decisionTree.AvgAbsoluteLoss, 0.2);


            var predictions = decisionTree.Predict(testSet.Select(x => x[1]).ToArray());

            Console.WriteLine($"Predictions with model [{decisionTree}]:");
            ConsoleExtensions.Print(predictions);
            Console.WriteLine();

            Assert.AreEqual(0, predictions[0], decisionTree.AvgAbsoluteLoss);
            Assert.AreEqual(-50, predictions[1], decisionTree.AvgAbsoluteLoss);
            Assert.AreEqual(-50, predictions[2], decisionTree.AvgAbsoluteLoss);
            Assert.AreEqual(10, predictions[3], decisionTree.AvgAbsoluteLoss);
        }

    }
}
